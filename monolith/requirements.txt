asgiref==3.5.2
black==22.10.0
certifi==2022.9.24
charset-normalizer==2.1.1
click==8.1.3
colorama==0.4.6
Django==4.1.3
gunicorn==20.1.0
idna==3.4
mypy-extensions==0.4.3
pathspec==0.10.2
pika==1.2.0
platformdirs==2.5.4
requests==2.28.1
sqlparse==0.4.3
tomli==2.0.1
tzdata==2022.6
urllib3==1.26.12
